package com.esi.chainurrents.users.application.dto;

import com.esi.chainurrents.common.ResourceSupport;
import lombok.Data;

/**
 * Created by gkgranada on 08/06/2017.
 */
@Data
public class UserDTO extends ResourceSupport{

    String email;
    String name;
    String address;
    Double currentBalance;
}
