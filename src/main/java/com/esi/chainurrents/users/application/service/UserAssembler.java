package com.esi.chainurrents.users.application.service;

import com.esi.chainurrents.bookings.rest.controller.BookingOrderRestController;
import com.esi.chainurrents.users.application.dto.UserDTO;
import com.esi.chainurrents.users.domain.model.User;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

/**
 * Created by gkgranada on 08/06/2017.
 */
@Service
public class UserAssembler  extends ResourceAssemblerSupport<User, UserDTO> {
    public UserAssembler() {
        super(BookingOrderRestController.class, UserDTO.class);
    }

    @Override
    public UserDTO toResource(User user) {

        UserDTO dto = createResourceWithId(user.getEmail(), user);
        dto.setEmail(user.getEmail());
        dto.setAddress(user.getAddress());
        dto.setName(user.getName());
        dto.setCurrentBalance(user.getCurrentBalance());

        return dto;
    }
}
