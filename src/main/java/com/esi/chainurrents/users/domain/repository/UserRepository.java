package com.esi.chainurrents.users.domain.repository;

import com.esi.chainurrents.properties.domain.model.PropertyReservation;
import com.esi.chainurrents.users.domain.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by gkgranada on 08/06/2017.
 */
@Repository
public interface UserRepository extends JpaRepository<User,String> {
    @Query("select i from User i where i.email = ?1")
    List<User> findUser(String orderId);

    @Query("select r from PropertyReservation r where (?1 < r.period.endDate) or (?2 > period.startDate)")
    List<PropertyReservation> findReservations(LocalDate startDate, LocalDate endDate);
}
