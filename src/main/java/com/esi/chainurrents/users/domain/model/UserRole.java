package com.esi.chainurrents.users.domain.model;

/**
 * Created by gkgranada on 08/06/2017.
 */
public enum UserRole {
    ROLE_GUEST,ROLE_HOST
}
