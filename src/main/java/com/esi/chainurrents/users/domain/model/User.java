package com.esi.chainurrents.users.domain.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by gkgranada on 08/06/2017.
 */
@Entity
@Getter
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
@AllArgsConstructor(staticName = "of")
public class User {
    @Id
    String email;

    String address;
    String name;

    @Column(precision = 8, scale = 2)
    Double currentBalance;

}
