package com.esi.chainurrents.common.application.exceptions;

/**
 * Created by gkgranada on 09/06/2017.
 */
public class PropertyNotFoundException extends Exception {
    public PropertyNotFoundException(String msg) {
        super(msg);
    }
}

