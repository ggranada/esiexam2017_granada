package com.esi.chainurrents.common.domain.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.persistence.Embeddable;
import java.time.LocalDate;

/**
 * Created by gkgranada on 08/06/2017.
 */
@Embeddable
@Value
@NoArgsConstructor(force=true,access= AccessLevel.PRIVATE)
@AllArgsConstructor(staticName="of")
public class ReservationPeriod {
    LocalDate startDate;
    LocalDate endDate;
}
