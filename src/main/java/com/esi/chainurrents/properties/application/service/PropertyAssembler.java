package com.esi.chainurrents.properties.application.service;

import com.esi.chainurrents.bookings.rest.controller.BookingOrderRestController;
import com.esi.chainurrents.common.ExtendedLink;
import com.esi.chainurrents.properties.application.dto.PropertyDTO;
import com.esi.chainurrents.properties.controller.PropertyRestController;
import com.esi.chainurrents.properties.domain.model.Property;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.http.HttpMethod.POST;

/**
 * Created by gkgranada on 08/06/2017.
 */
@Service
public class PropertyAssembler extends ResourceAssemblerSupport<Property, PropertyDTO> {
    public PropertyAssembler() {
        super(PropertyRestController.class, PropertyDTO.class);
    }

    @Override
    public PropertyDTO toResource(Property room) {
        PropertyDTO dto = createResourceWithId(room.get_id(),room);
        dto.set_id(room.get_id());
        dto.setPrice(room.getPrice());
        dto.setAddress(room.getAddress());
        dto.setAmenities(room.getAmenities());
        dto.setAreaInSqm(room.getAreaInSqm());
        dto.setFloor(room.getFloor());
        dto.setServices(room.getServices());
        return dto;
    }
}
