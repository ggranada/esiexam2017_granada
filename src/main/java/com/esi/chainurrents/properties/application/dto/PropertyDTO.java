package com.esi.chainurrents.properties.application.dto;

import com.esi.chainurrents.common.ResourceSupport;
import lombok.Data;

/**
 * Created by gkgranada on 08/06/2017.
 */
@Data
public class PropertyDTO extends ResourceSupport {

    String _id;
    Double price;
    String address;
    Double areaInSqm;
    int floor;
    String amenities;
    String services;
}
