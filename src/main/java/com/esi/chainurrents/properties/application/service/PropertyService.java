package com.esi.chainurrents.properties.application.service;

import com.esi.chainurrents.properties.application.dto.PropertyDTO;
import com.esi.chainurrents.properties.domain.repository.PropertyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by gkgranada on 08/06/2017.
 */
@Service
public class PropertyService {

    @Autowired
    PropertyRepository propertyRepository;

    @Autowired
    PropertyAssembler propertyAssembler;

    public List<PropertyDTO> findAvailableRooms(String address, LocalDate startDate, LocalDate endDate) {
        List<PropertyDTO> propertyDTOs = propertyAssembler.toResources(propertyRepository.findAvailable(address,startDate,endDate));
        return propertyDTOs;
        //return roomAssembler.toResources(propertyRepository.findAll());
    }
}
