package com.esi.chainurrents.properties.controller;

import com.esi.chainurrents.properties.application.dto.PropertyDTO;
import com.esi.chainurrents.properties.application.service.PropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Created by gkgranada on 08/06/2017.
 */
@RestController
@RequestMapping("/api/properties")
@CrossOrigin
public class PropertyRestController {

    @Autowired
    PropertyService propertyService;

    @GetMapping
    public List<PropertyDTO> findAvailableRooms (
            @RequestParam(name = "city", required = false) Optional<String> city,
            @RequestParam(name = "startDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> startDate,
            @RequestParam(name = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> endDate) {
        System.out.println("city: "+ city.get());
        if (city.isPresent() && startDate.isPresent() && endDate.isPresent()) {
            if (endDate.get().isBefore(startDate.get()))
                throw new IllegalArgumentException("Something wrong with the requested period ('endDate' happens before 'startDate')");
            return propertyService.findAvailableRooms(city.get(), startDate.get(), endDate.get());
        } else
            throw new IllegalArgumentException(
                    String.format("Wrong number of parameters: City='%s', Start date='%s', End date='%s'",
                            city.get(), startDate.get(), endDate.get()));
    }

}
