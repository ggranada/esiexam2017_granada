package com.esi.chainurrents.properties.domain.repository;

import com.esi.chainurrents.properties.domain.model.PropertyReservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by gkgranada on 08/06/2017.
 */
@Repository
public interface PropertyReservationRepository extends JpaRepository<PropertyReservation,String> {
    @Query("select i from PropertyReservation i where i.order._id = ?1")
    List<PropertyReservation> findReservationsUnderBooking(String orderId);

    @Query("select r from PropertyReservation r where (?1 < r.period.endDate) or (?2 > period.startDate)")
    List<PropertyReservation> findReservations(LocalDate startDate, LocalDate endDate);
}
