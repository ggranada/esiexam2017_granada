package com.esi.chainurrents.properties.domain.model;

/**
 * Created by gkgranada on 08/06/2017.
 */
public enum PropertyReservationStatus {
    CONFIRMED,REJECTED,CANCELLED,CLOSED
}
