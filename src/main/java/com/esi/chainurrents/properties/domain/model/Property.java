package com.esi.chainurrents.properties.domain.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by gkgranada on 08/06/2017.
 */
@Entity
@Data
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
@AllArgsConstructor(staticName="of")
public class Property {
    @Id
    String _id;

    @Column(precision = 8, scale = 2)
    Double price;

    String address;

    @Column(precision = 8, scale = 2)
    Double areaInSqm;

    int floor;

    String amenities;

    String services;
}
