package com.esi.chainurrents.properties.domain.repository;

import com.esi.chainurrents.properties.domain.model.Property;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by gkgranada on 08/06/2017.
 */
public interface CustomInventoryRepository {
    List<Property> findAvailable(String address, LocalDate startDate, LocalDate endDate);
}