package com.esi.chainurrents.properties.domain.model;

import com.esi.chainurrents.bookings.domain.model.BookingOrder;
import com.esi.chainurrents.common.domain.model.ReservationPeriod;
import com.esi.chainurrents.users.domain.model.User;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by gkgranada on 08/06/2017.
 */
@Entity
@Getter
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
@AllArgsConstructor(staticName = "of")
public class PropertyReservation {
    @Id
    String _id;

    @ManyToOne
    Property property;

    @Embedded
    ReservationPeriod period;

    @ManyToOne
    User user;

    @Enumerated(EnumType.STRING)
    PropertyReservationStatus status;

    @OneToOne
    BookingOrder order;

    public void handleCancellation() {
        status = PropertyReservationStatus.CANCELLED;
    }
    public void handleClosing() {
        status = PropertyReservationStatus.CLOSED;
    }
}
