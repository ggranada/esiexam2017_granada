package com.esi.chainurrents.properties.domain.repository;

import com.esi.chainurrents.properties.domain.model.Property;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by gkgranada on 08/06/2017.
 */
@Repository
public interface PropertyRepository extends JpaRepository<Property, String>,CustomInventoryRepository {
    @Query("select i from Property i where ?1 in i.address and i not in (select r.property from PropertyReservation r where (?2 between r.period.startDate and r.period.endDate) and (?3 between period.startDate and period.endDate))")
    List<Property> findAvailable(String address, LocalDate startDate, LocalDate endDate);

//List<Room> findAvailable(LocalDate startDate, LocalDate endDate);

}