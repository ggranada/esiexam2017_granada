package com.esi.chainurrents.properties.infrastructure;

import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * Created by gkgranada on 08/06/2017.
 */
@Service
public class PropertyReservationIdentifierFactory {
        public String nextPropertyReservationId() {
            return UUID.randomUUID().toString();
        }
}