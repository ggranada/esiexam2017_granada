package com.esi.chainurrents.bookings.domain.model;

import com.esi.chainurrents.common.domain.model.ReservationPeriod;
import com.esi.chainurrents.properties.domain.model.Property;
import com.esi.chainurrents.users.domain.model.User;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;

/**
 * Created by gkgranada on 08/06/2017.
 */
@Entity
@Data
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
@AllArgsConstructor(staticName="of")
public class BookingOrder {
    @Id
    String _id;

    @ManyToOne
    User user;

    @ManyToOne
    Property property;

    @Embedded
    ReservationPeriod period;


    @Enumerated(EnumType.STRING)
    BookingOrderStatus status;

    public void handleCancellation() {
        status = BookingOrderStatus.CANCELLED;
    }
    public void handleClosing() {
        status = BookingOrderStatus.CLOSED;
    }

    public void handleRejection() {
        status = BookingOrderStatus.REJECTED;
    }
}