package com.esi.chainurrents.bookings.domain.model;

/**
 * Created by gkgranada on 08/06/2017.
 */
public enum BookingOrderStatus {
    CONFIRMED,CLOSED,REJECTED,CANCELLED
}
