package com.esi.chainurrents.bookings.domain.repository;

import com.esi.chainurrents.bookings.domain.model.BookingOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by gkgranada on 08/06/2017.
 */
@Repository
public interface BookingOrderRepository extends JpaRepository<BookingOrder,String> {
}