package com.esi.chainurrents.bookings.application.dto;

import com.esi.chainurrents.bookings.domain.model.BookingOrderStatus;
import com.esi.chainurrents.common.ResourceSupport;
import com.esi.chainurrents.common.application.dto.ReservationPeriodDTO;
import com.esi.chainurrents.properties.application.dto.PropertyDTO;
import com.esi.chainurrents.properties.domain.model.Property;
import com.esi.chainurrents.users.application.dto.UserDTO;
import lombok.Data;

import java.util.ArrayList;

/**
 * Created by gkgranada on 08/06/2017.
 */
@Data
public class BookingOrderDTO extends ResourceSupport {
    String _id;
    UserDTO user;
    PropertyDTO property;
    ReservationPeriodDTO period;
    BookingOrderStatus status;
}
