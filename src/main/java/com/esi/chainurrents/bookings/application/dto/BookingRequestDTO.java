package com.esi.chainurrents.bookings.application.dto;

import com.esi.chainurrents.common.ResourceSupport;
import com.esi.chainurrents.common.application.dto.ReservationPeriodDTO;
import com.esi.chainurrents.users.application.dto.UserDTO;
import lombok.Data;

/**
 * Created by gkgranada on 09/06/2017.
 */
@Data
public class BookingRequestDTO extends ResourceSupport {
    String _id;
    String room_id;
    UserDTO user;
    ReservationPeriodDTO period;
}
