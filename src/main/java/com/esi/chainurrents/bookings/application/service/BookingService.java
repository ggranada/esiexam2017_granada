package com.esi.chainurrents.bookings.application.service;

import com.esi.chainurrents.bookings.application.dto.BookingOrderDTO;
import com.esi.chainurrents.bookings.domain.model.BookingOrder;
import com.esi.chainurrents.bookings.domain.model.BookingOrderStatus;
import com.esi.chainurrents.bookings.domain.repository.BookingOrderRepository;
import com.esi.chainurrents.bookings.infrastructure.BookingOrderIdentifierFactory;
import com.esi.chainurrents.bookings.infrastructure.BookingRequestIdentifierFactory;
import com.esi.chainurrents.common.application.exceptions.PropertyNotFoundException;
import com.esi.chainurrents.common.domain.model.ReservationPeriod;
import com.esi.chainurrents.properties.application.service.PropertyService;
import com.esi.chainurrents.properties.domain.model.Property;
import com.esi.chainurrents.properties.domain.model.PropertyReservation;
import com.esi.chainurrents.properties.domain.model.PropertyReservationStatus;
import com.esi.chainurrents.properties.domain.repository.PropertyRepository;
import com.esi.chainurrents.properties.domain.repository.PropertyReservationRepository;
import com.esi.chainurrents.properties.infrastructure.PropertyReservationIdentifierFactory;
import com.esi.chainurrents.users.domain.model.User;
import com.esi.chainurrents.users.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.time.temporal.ChronoUnit.DAYS;

/**
 * Created by gkgranada on 08/06/2017.
 */
@Service
public class BookingService {
    @Autowired
    BookingOrderIdentifierFactory orderIdentifierFactory;

    @Autowired
    BookingRequestIdentifierFactory requestIdentifierFactory;

    @Autowired
    PropertyReservationIdentifierFactory reservationIdFactory;

    @Autowired
    PropertyRepository propertiesRepository;

    @Autowired
    PropertyReservationRepository reservationRepository;

    @Autowired
    BookingOrderRepository orderRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PropertyService propertyService;

    @Autowired
    BookingOrderAssembler orderAssembler;

    public BookingOrder createBookingOrder(BookingOrderDTO requestDTO) throws PropertyNotFoundException {
        List<Property> availableProperties = propertiesRepository.findAvailable(requestDTO.getProperty().getAddress(),requestDTO.getPeriod().getStartDate(),requestDTO.getPeriod().getEndDate());
        System.out.println("properties: "+availableProperties);

        ReservationPeriod period = ReservationPeriod.of(
                requestDTO.getPeriod().getStartDate(),
                requestDTO.getPeriod().getEndDate());

        long numberOfDays = DAYS.between(period.getStartDate(),period.getEndDate());
        System.out.println("days: "+ numberOfDays);

        double totalPrice = numberOfDays * requestDTO.getProperty().getPrice();
        System.out.println("totalPrice: "+ totalPrice);

        User user = userRepository.findOne(requestDTO.getUser().getEmail());
        Property property = Property.of(
                requestDTO.getProperty().get_id(),
                requestDTO.getProperty().getPrice(),
                requestDTO.getProperty().getAddress(),
                requestDTO.getProperty().getAreaInSqm(),
                requestDTO.getProperty().getFloor(),
                requestDTO.getProperty().getAmenities(),
                requestDTO.getProperty().getServices()
                );
        BookingOrder order = BookingOrder.of(
                orderIdentifierFactory.nextBookingOrderId(),
                user,
                property,
                period,
                BookingOrderStatus.CONFIRMED);
        if (!availableProperties.contains(property)) {
            order.handleRejection();
            orderRepository.save(order);
            throw new PropertyNotFoundException("Property is not available.");
        }
        if (user.getCurrentBalance() < (totalPrice/2)) {
            order.handleRejection();
            orderRepository.save(order);
            throw new PropertyNotFoundException("User Balance is not enough.");
        }
        orderRepository.save(order);
        PropertyReservation reservation = PropertyReservation.of(
                reservationIdFactory.nextPropertyReservationId(),
                   property, period, user, PropertyReservationStatus.CONFIRMED,order
            );


        return order;
    }

    public BookingOrderDTO cancelBookingOrder(String id) {
        BookingOrder order = orderRepository.findOne(id);
        order.handleCancellation();
        List<PropertyReservation> reservationList = reservationRepository.findReservationsUnderBooking(id);
        for (PropertyReservation reservation : reservationList) {
            reservation.handleCancellation();
            reservationRepository.save(reservation);
        }
        return orderAssembler.toResource(orderRepository.save(order));
    }

    public BookingOrderDTO closeBookingOrder(String id) {
        BookingOrder order = orderRepository.findOne(id);
        order.handleClosing();
        List<PropertyReservation> reservationList = reservationRepository.findReservationsUnderBooking(id);
        for (PropertyReservation reservation : reservationList) {
            reservation.handleClosing();
            reservationRepository.save(reservation);
        }
        return orderAssembler.toResource(orderRepository.save(order));
    }
}
