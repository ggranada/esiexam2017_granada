package com.esi.chainurrents.bookings.application.service;


import com.esi.chainurrents.bookings.application.dto.BookingOrderDTO;
import com.esi.chainurrents.bookings.domain.model.BookingOrder;
import com.esi.chainurrents.bookings.rest.controller.BookingOrderRestController;
import com.esi.chainurrents.common.ExtendedLink;
import com.esi.chainurrents.common.application.dto.ReservationPeriodDTO;
import com.esi.chainurrents.properties.application.service.PropertyAssembler;
import com.esi.chainurrents.users.application.service.UserAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.http.HttpMethod.DELETE;

/**
 * Created by gkgranada on 08/06/2017.
 */
@Service
public class BookingOrderAssembler  extends ResourceAssemblerSupport<BookingOrder, BookingOrderDTO> {

    @Autowired
    UserAssembler userAssembler;

    @Autowired
    PropertyAssembler propertyAssembler;

    public BookingOrderAssembler() {
        super(BookingOrderRestController.class, BookingOrderDTO.class);
    }

    @Override
    public BookingOrderDTO toResource(BookingOrder order) {
        ReservationPeriodDTO periodDTO = new ReservationPeriodDTO();
        periodDTO.setStartDate(order.getPeriod().getStartDate());
        periodDTO.setEndDate(order.getPeriod().getEndDate());

        BookingOrderDTO dto = createResourceWithId(order.get_id(), order);
        dto.set_id(order.get_id());
        dto.setPeriod(periodDTO);
        dto.setUser(userAssembler.toResource(order.getUser()));
        dto.setProperty(propertyAssembler.toResource(order.getProperty()));
        dto.setStatus(order.getStatus());

        try {
            switch (dto.getStatus()) {
                case CONFIRMED:
                    dto.add(new ExtendedLink(
                            linkTo(methodOn(BookingOrderRestController.class)
                                    .cancelBookingOrder(dto.get_id())).toString(),
                            "cancel", DELETE));
                    dto.add(new ExtendedLink(
                            linkTo(methodOn(BookingOrderRestController.class)
                                    .closeBookingOrder(dto.get_id())).toString(),
                            "close", DELETE));
                    break;

                default:
                    break;
            }
        } catch (Exception e) {
        }
        return dto;
    }


}