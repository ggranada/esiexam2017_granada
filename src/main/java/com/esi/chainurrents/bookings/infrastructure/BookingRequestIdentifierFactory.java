package com.esi.chainurrents.bookings.infrastructure;

import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * Created by gkgranada on 08/06/2017.
 */
@Service
public class BookingRequestIdentifierFactory {
        public String nextBookingRequestId() {
            return UUID.randomUUID().toString();
        }
}