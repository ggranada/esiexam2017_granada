package com.esi.chainurrents.bookings.rest.controller;

import com.esi.chainurrents.bookings.application.dto.BookingOrderDTO;
import com.esi.chainurrents.bookings.application.service.BookingOrderAssembler;
import com.esi.chainurrents.bookings.application.service.BookingService;
import com.esi.chainurrents.bookings.domain.model.BookingOrder;
import com.esi.chainurrents.common.application.exceptions.PropertyNotFoundException;
import com.esi.chainurrents.properties.application.service.PropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * Created by gkgranada on 08/06/2017.
 */
@RestController
@RequestMapping("/api/orders")
@CrossOrigin
public class BookingOrderRestController {

    @Autowired
    PropertyService propertyService;

    @Autowired
    BookingService bookingService;

    @Autowired
    BookingOrderAssembler bookingOrderAssembler;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public BookingOrderDTO createBookingOrder (@RequestBody Optional<BookingOrderDTO> partialDto) throws PropertyNotFoundException{
        BookingOrderDTO request = partialDto.get();
        System.out.println("REQUEST: " + request);
        BookingOrder order = bookingService.createBookingOrder(request);
        System.out.println("BookingOrder: " + order);

        return bookingOrderAssembler.toResource(order);
    }


    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public BookingOrderDTO cancelBookingOrder(@PathVariable String id) throws Exception {
        return bookingService.cancelBookingOrder(id);
    }

    @DeleteMapping("/{id}/close")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public BookingOrderDTO closeBookingOrder(@PathVariable String id) throws Exception {
        return bookingService.closeBookingOrder(id);
    }

    @ExceptionHandler(PropertyNotFoundException.class)
    @ResponseStatus(HttpStatus.PRECONDITION_FAILED)
    public String bindPurchaseExceptionHandler(Exception ex) {
        return ex.getMessage();
    }
}
