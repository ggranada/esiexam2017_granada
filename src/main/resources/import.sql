insert into user (email, address, name, current_Balance) values ('admin@hotel.com', 'tartu', 'Admin Hotel', 10000);
insert into user (email, address, name, current_Balance) values ('guest@hotel.com', 'tallinn', 'Guest Hotel', 5000);
insert into user (email, address, name, current_Balance) values ('employee@hotel.com', 'narva', 'Employee Hotel', 20);


insert into property (_id, address, amenities, area_In_Sqm, floor, price, services) values ('1-1', 'tallinn', 'linens', 50, 5, 30, '3 beds');
insert into property (_id, address, amenities, area_In_Sqm, floor, price, services) values ('1-2', 'tartu', 'linens', 50, 5, 15, '1 bed');
insert into property (_id, address, amenities, area_In_Sqm, floor, price, services) values ('1-3', 'tallinn', 'linens', 50, 5, 100, '3 beds with kitchen');
insert into property (_id, address, amenities, area_In_Sqm, floor, price, services) values ('1-4', 'tartu', 'linens', 50, 5, 30, 'Studio');
insert into property (_id, address, amenities, area_In_Sqm, floor, price, services) values ('1-5', 'tallinn', 'linens', 50, 5, 100, '3 beds with kitchen');
insert into property (_id, address, amenities, area_In_Sqm, floor, price, services) values ('1-6', 'tartu', 'linens', 50, 5, 30, '3 beds');
insert into property (_id, address, amenities, area_In_Sqm, floor, price, services) values ('1-7', 'tallinn', 'linens', 50, 5, 100, '3 beds with kitchen');
insert into property (_id, address, amenities, area_In_Sqm, floor, price, services) values ('1-8', 'tartu', 'linens', 50, 5, 100, '3 beds with kitchen');
insert into property (_id, address, amenities, area_In_Sqm, floor, price, services) values ('1-9', 'tallinn', 'linens', 50, 5, 40, '2 beds');
insert into property (_id, address, amenities, area_In_Sqm, floor, price, services) values ('1-10', 'tartu', 'linens', 50, 5, 30, '3 beds');

insert into property_reservation (_id, property__id, start_date, end_date, user_email, status) values (1, '1-5', '2017-03-22', '2017-03-24', 'guest@hotel.com', 'CONFIRMED');
